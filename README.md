# Coding Factory Header for VSCode

This extension provides the CodingFactory header integration in VS Code.

```bash
********************************************************************************
*                                                                              *
*                                                        ██████╗███████╗       *
*    vscode-codingfactoryheader.                        ██╔════╝██╔════╝       *
*                                                       ██║     █████╗         *
*    By: erucquoy                                       ██║     ██╔══╝         *
*                                                       ╚██████╗██║            *
*    Created: 1/1/1990 08:42:19 by kube                  ╚═════╝╚═╝            *
*    Updated: 1/12/2021 07:01:42 by erucquoy   for    codingfactory.fr         *
*                                                                              *
********************************************************************************

```

## Install

Launch Quick Open with <kbd>⌘</kbd>+<kbd>P</kbd> and enter
```
ext install codingfactoryheader
```

## Usage

### Insert a header
 - **macOS** : <kbd>⌘</kbd> + <kbd>⌥</kbd> + <kbd>H</kbd>
 - **Linux** / **Windows** : <kbd>Ctrl</kbd> + <kbd>Alt</kbd> + <kbd>H</kbd>.

Header is automatically updated on save.


## Configuration

Default values for **username** and **email** are imported from environment variables.

To override these values, specify these properties in *User Settings* :

```ts
{
  "codingfactoryheader.username": string,
  "codingfactoryheader.email": string
}
```


## Issues

In case of a bug, or missing feature, please create a [Github Pull Request](https://gitlab.com/erucquoy/vscode-codingfactoryheader/pulls).

## License

MIT
